﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ExMYsql
{
    public partial class FrmConnectDatabase : Form
    {
        public string ServerName ;
        public string DatabaseName;
        public string UserName;
        public string Password;
        public string ConnectStr;
        public FrmConnectDatabase()
        {
            InitializeComponent();
            txtServerName.Text = Properties.Settings.Default.ServerName;
            txtDatabaseName.Text = Properties.Settings.Default.DatabaseName;
            txtUerName.Text = Properties.Settings.Default.UserName;
            txtPassword.Text = Properties.Settings.Default.Password;
            ConnectStr = Properties.Settings.Default.ConnectStr;
        }

        private void BtCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtSave_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.ServerName = txtServerName.Text;
            Properties.Settings.Default.DatabaseName = txtDatabaseName.Text;
            Properties.Settings.Default.UserName = txtUerName.Text;
            Properties.Settings.Default.Password = txtPassword.Text;
            Properties.Settings.Default.ConnectStr = $"Server={ txtServerName.Text};"
                + $"Database={txtDatabaseName.Text};"
                + $"Uid={txtUerName.Text};"
                + $"Pwd={txtPassword.Text};"; 

            Properties.Settings.Default.Save();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Enter:
                    btSave.Focus();
                    BtSave_Click(null,EventArgs.Empty);
                    break;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
