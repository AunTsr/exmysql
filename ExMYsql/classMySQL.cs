﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using NUnit;
using NUnit.Framework;

namespace ExMYsql
{
    public class ClassMySQL
    {
        public string ServerName { get; set; }
        public string DatabaseName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Con { get; set; }
        MySqlConnection mySql;

        enum ConnectionStatus
        {
            Connected,
            NotConnect
        }

        public ClassMySQL()
        {

        }

        public ClassMySQL(string Server, string Database, string User, string Password)
        {
            this.ServerName = Server;
            this.DatabaseName = Database;
            this.UserName = User;
            this.Password = Password;
            this.Con = $"Server={Server};Database={Database};Uid={User};Pwd={Password};";
        }

        public bool Connect()
        {
            try
            {
                mySql = new MySqlConnection(this.Con);
                mySql.Open();
                mySql.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DisConnect()
        {
            try
            {
                mySql.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public DataTable CallStore(string Store, List<string> Params)
        {
            string Cmd = $"call {Store} (";

            for (int i = 0; i < 20; i++)
            {
                if (Params.Count - 1 >= i) Cmd = String.Concat(Cmd, "\'" + Params[i] + "\'" + ",");
                else Cmd = String.Concat(Cmd, "\'" + "" + "\'" + ",");
            }

            Cmd = Cmd.Substring(0, Cmd.Length - 1) + ");";

            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataAdapter Val = new MySqlDataAdapter(Cmd, mySql);
                DataTable dt = new DataTable();
                Val.Fill(dt);
                mySql.Close();
                Command.Dispose();
                Val.Dispose();
                return dt;
            }
            catch
            {
                return new DataTable();
            }
        }

        public DataTable SelectDataAll(string Table)
        {
            string Cmd = $"Select * from {Table};";
            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataAdapter Val = new MySqlDataAdapter(Cmd, mySql);
                DataTable dt = new DataTable();
                Val.Fill(dt);
                mySql.Close();
                Command.Dispose();
                Val.Dispose();
                return dt;
            }
            catch
            {
                return new DataTable();
            }
        }

        public DataTable SelectData(string Query)
        {
            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Query, mySql);
                MySqlDataAdapter Val = new MySqlDataAdapter(Query, mySql);
                DataTable dt = new DataTable();
                Val.Fill(dt);
                mySql.Close();
                Command.Dispose();
                Val.Dispose();
                return dt;
            }
            catch
            {
                return new DataTable();
            }
        }

        public DataTable SelectData(string Table, List<string> ColName)
        {
            string CmdBegin = "Select";
            string CmdEnd = $"From {Table}";
            for (int i = 0; i < ColName.Count; i++)
            {
                CmdBegin = String.Concat(CmdBegin, ColName[i] + ",");
            }

            string Cmd = CmdBegin.Substring(0, CmdBegin.Length - 1) + CmdEnd + ";";

            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataAdapter Val = new MySqlDataAdapter(Cmd, mySql);
                DataTable dt = new DataTable();
                Val.Fill(dt);
                mySql.Close();
                Command.Dispose();
                Val.Dispose();
                return dt;
            }
            catch
            {
                return new DataTable();
            }
        }

        public bool InsertData(string Table, List<string> ColName, List<string> Value)
        {
            string CmdBegin = $"Insert Into {Table} (";
            string CmdEnd = $"Values (";
            for (int i = 0; i < ColName.Count; i++)
            {
                CmdBegin = String.Concat(CmdBegin, ColName[i].ToString() + ",");
                CmdEnd = String.Concat(CmdEnd, "\'" + Value[i].ToString() + "\'" + ",");
            }

            string Cmd = CmdBegin.Substring(0, CmdBegin.Length - 1) + ")" + CmdEnd.Substring(0, CmdEnd.Length - 1) + ");";

            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataReader MyReader;
                MyReader = Command.ExecuteReader();
                while (MyReader.Read())
                {

                }
                mySql.Close();
                Command.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateData(string Table, List<string> ColName, List<string> Value, string Where)
        {
            string CmdBegin = $"UPDATE  {Table} SET (";
            string CmdEnd = $"WHERE {Where}";
            for (int i = 0; i < ColName.Count; i++)
            {
                CmdBegin = String.Concat(CmdBegin, "`" + ColName[i] + "`" + "=" + "\"" + Value[i] + "\"");
            }

            string Cmd = CmdBegin.Substring(0, CmdBegin.Length - 1) + ")" + CmdEnd + ";";

            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataReader MyReader;
                MyReader = Command.ExecuteReader();
                while (MyReader.Read())
                {

                }
                mySql.Close();
                Command.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteData(string Table, string Where)
        {
            string Cmd = $"DELETE FROM {Table} WHERE {Where};";

            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataReader MyReader;
                MyReader = Command.ExecuteReader();
                while (MyReader.Read())
                {

                }
                mySql.Close();
                Command.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
